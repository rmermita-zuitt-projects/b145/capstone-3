import React from 'react';

import { Container, Row, Col, Button } from 'react-bootstrap'

import { NavLink } from 'react-router-dom'

import { Fragment } from 'react';

import BootstrapCarouselComponent from './BootstrapCarousel';

import 'bootstrap/dist/css/bootstrap.min.css';

export default function Banner() {
	return (
		<Fragment>
			<div className="Banner">
				<BootstrapCarouselComponent></BootstrapCarouselComponent>
			</div>
			<div className="d-flex justify-content-center px-5">
				<Button className="btn" variant="outline-secondary" size="mx" as={NavLink} to="/products">Order Now!</Button>
			</div>
		</Fragment>
	)
}