import { Fragment, useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import React from 'react'
import UserContext from '../UserContext'

import { Navigate, useNavigate } from 'react-router-dom'

import Swal from 'sweetalert2'

import { Link } from 'react-router-dom'

export default function Register() {

	const { user } = useContext(UserContext)

	const history = useNavigate()

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [password, setpassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	function registerUser(e) {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password,
				verifyPassword: verifyPassword
			})
		})
			.then(res => res.json())
			.then(data => {
				if (data.success === true) {

					setFirstName('')
					setLastName('')
					setMobileNo('')
					setEmail('')
					setpassword('')
					setVerifyPassword('')

					Swal.fire({
						title: data.title,
						icon: 'success',
						text: data.text
					})

					history("/login")

				} else {
					Swal.fire({
						title: data.title,
						icon: 'error',
						text: data.text
					})
				}
			})

		setFirstName('')
		setLastName('')
		setMobileNo('')
		setEmail('')
		setpassword('')
		setVerifyPassword('')
	}

	useEffect(() => {
		if ((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password, verifyPassword])


	return (

		(user.id !== null) ?

			<Navigate to="/login" />
			:

			<Fragment>
				<h1 className="mt-4">Register</h1>
				<Form onSubmit={(e) => registerUser(e)}>
					<Form.Group controlId="firstName">
						<Form.Label>
							First Name
						</Form.Label>
						<Form.Control

							type='text' placeholder='Enter your first name here' value={firstName} onChange={e => setFirstName(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="lastName">
						<Form.Label>
							Last Name
						</Form.Label>
						<Form.Control

							type='text' placeholder='Enter your last name here' value={lastName} onChange={e => setLastName(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="mobileNo">
						<Form.Label>
							Mobile Number
						</Form.Label>
						<Form.Control

							type='text' placeholder='Enter your mobile number here' value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="userEmail">
						<Form.Label>
							Email Address
						</Form.Label>
						<Form.Control

							type='email' placeholder='Enter your email here' value={email} onChange={e => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>
							We will never share your email with anyone else.
						</Form.Text>
					</Form.Group>
					<Form.Group controlId="password">
						<Form.Label>Password</Form.Label>
						<Form.Control type='password' placeholder='Enter your password here' value={password} onChange={e => setpassword(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="verifyPassword">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control type='password' placeholder='Re-Enter your password here' value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required />
					</Form.Group>

					{isActive ?
						<Button variant="success" type="submit" id="submitBtn" className="mt-3">
							Submit
						</Button>

						:

						<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
							Submit
						</Button>

					}
				</Form>
				<h5>Already registered? <Link to="/login">Click here!</Link></h5>
			</Fragment>
	)
}
